<?php

class Users extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        
    }
    public function create(){
        
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('user_name', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required[valid_email]');
        $this->form_validation->set_rules('mobile', 'Phone number', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == false) {
            $this->layout->view_layout('create');
        } else {
            $formArray = array();
            $formArray['name'] = $this->input->post('name');
            $formArray['user_name'] = $this->input->post('user_name');
            $formArray['email'] = $this->input->post('email');
            $formArray['mobile'] = $this->input->post('mobile');
            $formArray['password'] = $this->input->post('password');
            $this->User_model->create($formArray);
            $this->session->set_flashdata('success', 'Record add successfully!');
            redirect(base_url().'users/index');
        }
    }
    public function index() 
    {
        $users = $this->User_model->all();
        $data = array();
        $data['users'] = $users;
        $this->layout->view_layout('list',$data);
       
    }
    public function edit($userId)
    {
        $user = $this->User_model->getUser($userId);
        $data = array();
        $data['user'] = $user;

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('user_name', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required[valid_email]');
        $this->form_validation->set_rules('mobile', 'Phone number', 'required');
        
        if ($this->form_validation->run() == false ) {
            $this->layout->view_layout('edit', $data);
        } else {
            $formArray = array();
            $formArray['name'] = $this->input->post('name');
            $formArray['user_name'] = $this->input->post('user_name');
            $formArray['email'] = $this->input->post('email');
            $formArray['mobile'] = $this->input->post('mobile');
            $this->User_model->updateUser($userId, $formArray);
            $this->session->set_flashdata('success', 'Record update successfully!');
            redirect(base_url().'users/index');
        }
    }
    public function delete($userId)
    {
        $user = $this->User_model->getUser($userId);

        if (empty($user)){
            $this->session->set_flashdata('failure', 'Record not found!');
            redirect(base_url().'users/index');
        } else {
            $this->User_model->deleteUser($userId);
            $this->session->set_flashdata('success', 'Record delete successfully!');
            redirect(base_url().'users/index');
        }
        
    }
        
}


?>