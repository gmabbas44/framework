<?php

class User_model extends CI_Model {

    public function create($formArray)
    {
        $this->db->insert('user_info',$formArray);
    }
    public function all()
    {
        return $this->db->get('user_info')->result_array();
        // result() for fetch data with object
        // result_array() for fetch data with assocative array
    }
    public function getUser($id)
    {
        $this->db->where('id',$id);
        return $this->db->get('user_info')->row_array();
    }
    public function updateUser($user_id, $formArray)
    {
        $this->db->where('id',$user_id);
        $this->db->update('user_info', $formArray);
    }
    public function deleteUser($userId)
    {
        $this->db->where('id',$userId);
        $this->db->delete('user_info');
    }
}


?>