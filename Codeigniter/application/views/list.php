

<div class="container mt-5">
    <?php
        $msg = $this->session->userdata('success');
        if ($msg):
    ?>
    <div class="alert alert-success">
        <?= $msg; ?>
    </div>
    <?php endif; ?>
    
    <?php
        $failure = $this->session->userdata('failure');
        if ($failure) :
    ?>
    <div class="alert alert-warning">
        <?= $failure; ?>
    </div>
    <?php endif; ?>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary">
                <div class="card-header"> Show Users <a class="btn btn-sm btn-primary float-right" href="<?= base_url() ;?>users/create"> Create</a></div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            <?php if ($users > 0) :
                                foreach ($users as $user) :
                            ?>
                            <tr>
                                <td><?= $user['id']; ?></td>
                                <td><?= $user['name']; ?></td>
                                <td><?= $user['user_name']; ?></td>
                                <td><?= $user['email']; ?></td>
                                <td><?= $user['mobile']; ?></td>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="<?= base_url(). 'users/edit/'.$user['id'] ; ?>">Edit</a>
                                    <a class="btn btn-danger btn-sm" href="<?= base_url(). 'users/delete/'.$user['id'] ; ?>">Delete</a>
                                </td>
                            </tr>
                        <?php
                            endforeach;
                            else :
                                echo "<tr><td class='text-center text-danger' colspan='5'>No data here</td></tr>";
                            endif;
                         ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

