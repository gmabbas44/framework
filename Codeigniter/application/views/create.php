

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6 ">
            <div class="card border-primary">
                <div class="card-header"> Create User </div>
                <div class="card-body">
                    <form name="createUser" method="post" action="<?= base_url().'users/create'?>">
                        <div class="form-group">
                            <input type="text" name="name" value="<?= set_value('name') ; ?>" id="" class="form-control" placeholder="Enter your name">
                            <small id="emailHelp" class="form-text text-muted"><?= form_error('name') ; ?></small>
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <input type="text" name="user_name" value="<?= set_value('user_name') ; ?>" id="" class="form-control" placeholder="Enter your Username">
                            <small id="emailHelp" class="form-text text-muted"><?= form_error('user_name') ; ?></small>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" value="<?= set_value('email') ; ?>" id="" class="form-control" placeholder="Enter your email">
                            <small id="emailHelp" class="form-text text-muted"><?= form_error('email') ; ?></small>
                        </div>
                        <div class="form-group">
                            <input type="number"  maxlength="14" name="mobile" value="<?= set_value('mobile') ; ?>" id="" class="form-control" placeholder="Enter your mobile">
                            <small id="emailHelp" class="form-text text-muted"><?= form_error('mobile') ; ?></small>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="" class="form-control" placeholder="Enter your Password">
                            <small id="emailHelp" class="form-text text-muted"><?= form_error('password') ; ?></small>
                        </div>
                        <button type="submit" id="" class="btn btn-primary btn-block">submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>

