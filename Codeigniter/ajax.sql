-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 08:17 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajax`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `author` varchar(50) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `author`, `comment`) VALUES
(1, 'Gm Abbas Uddin', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, '),
(2, 'Khaleda', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, '),
(3, 'nodi', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore quam, numquam ab expedita asperiores maiores aut repellendus odio, temporibus praesentium corrupti explicabo in, possimus laudantium debitis aliquid culpa! Ipsa, nisi?'),
(4, 'Khaleda', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore quam, numquam ab expedita asperiores maiores aut repellendus odio, temporibus praesentium corrupti explicabo in, possimus laudantium debitis aliquid culpa! Ipsa, nisi?'),
(5, 'Afifa Jannat', 'A paragraph is a self-contained unit of a discourse in writing dealing with a particular point or idea. A paragraph consists of one or more sentences.'),
(6, 'Khaleda Akter Nodi', 'IEP students attend classes 20 hours or more per week. Classes are every day, usually from morning through mid-afternoon.'),
(7, 'abbas', 'This course is designed to develop basic English language reading, writing, and vocabulary skills for students who have not had prior experience with the English language.'),
(8, 'Gm Abbas uddin', ' Students learn to name and form all letters and numbers, to write the most common words with correct spelling and to follow the rules of capitalization and punctuation. '),
(9, 'My Life', ' Students also develop reading strategies including recognizing common sight words, decoding all alphabet sounds, and reading and responding appropriately to words and sentences. '),
(10, 'Khaleda Akter Nodi', 'This course is designed to help novice speakers of English to develop grammar concepts. Students learn to recognize basic parts of speech and construct simple sentences using present and present progressive tenses');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` int(11) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `name`, `user_name`, `email`, `mobile`, `password`) VALUES
(1, 'abbas', 'gmabbas44', 'gmabbas44@gmail.com', 1981781661, '1234'),
(2, 'Gm abbas Uddin', 'gmabbas44', 'gmabbas44@gmail.com', 5211, '12345678'),
(5, 'Khaleda Akter Nodi', 'Khaledacse', 'khaledaakter382616@gmail.com', 1814953905, '123456'),
(6, 'Khaleda Akter Nodi', 'Khaledacse', 'khaledaakter382616@gmail.com', 1814953905, '123456'),
(7, 'Gm abbas Uddin', 'gmabbas44', 'gmabbas44@gmail.com', 1845109044, '123'),
(8, 'Gm abbas Uddin', 'gmabbas44', 'gmabbas44@gmail.com', 123456, '123'),
(9, 'Khaleda Akter Nodi', 'Khaledacse', 'khaledaakter382616@gmail.com', 1814953905, '123456'),
(11, 'Gm Abbas Uddin', 'gmabbas44', 'gmabbas44@gmail.com', 123456, '123');

-- --------------------------------------------------------

--
-- Table structure for table `_user`
--

CREATE TABLE `_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_pass` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_user`
--

INSERT INTO `_user` (`id`, `user_name`, `user_pass`) VALUES
(1, 'Gm Abbas Uddin', '123456'),
(2, 'abbas', '123'),
(3, 'MD. Abbas Uddin', ''),
(4, 'MD. Abbas Uddin', '123456'),
(5, 'Khaleda Akter', '123456'),
(6, 'Gm Abbas Uddin', '123456'),
(7, 'Nodi', '123'),
(8, 'Gm Abbas Uddin', 'Khaleda Akter Nodi'),
(9, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_user`
--
ALTER TABLE `_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `_user`
--
ALTER TABLE `_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
