<form action="" method="post" id="createFrom" name="createFrom">
    <div class="form-group">
        <label for="name" class="sr-only">Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Enter your name" aria-describedby="helpId">
        <small id="helpId" class="text-muted"><?= form_error('name') ; ?></small>
    </div>
    <div class="form-group">
        <label for="user_name" class="sr-only">User Name</label>
        <input type="text" name="user_name" id="user_name" class="form-control" placeholder="Enter your username" aria-describedby="helpId">
        <small id="helpId" class="text-muted"><?= form_error('user_name') ; ?></small>
    </div>
    <div class="form-group">
        <label for="email" class="sr-only">Email</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="Enter your Email" aria-describedby="helpId">
        <small id="helpId" class="text-muted"><?= form_error('email') ; ?></small>
    </div>
    <div class="form-group">
        <label for="mobile" class="sr-only">Mobile</label>
        <input type="number" name="mobile" id="mobile" class="form-control" placeholder="Enter your Mobile" aria-describedby="helpId">
        <small id="helpId" class="text-muted"><?= form_error('mobile') ; ?></small>
    </div>
    <div class="from-group">
        <lable for="password" class="sr-only">Password</lable>
        <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
        <small id="helpId" class="text-muted"><?= form_error('password') ; ?></small>
    </div>
    <div class="from-group float-right mt-2">
    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    <button type="submit" id="btn-submit" class="btn btn-primary">Save</button>
    </div>
    
</form>