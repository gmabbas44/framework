

	<div class="container">
		<div class="card">
			<div class="card-header">
				<h3 class="float-left">All User</h3>
				<a class="btn btn-primary float-right create-btn" data-toggle="modal" data-toggle="#createModal" href="<?= base_url()?>index.php/users/create"> Create</a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped table-inverse">
						<thead class="thead-inverse">
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Username</th>
								<th>Email</th>
								<th>Mobile</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="row"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td scope="row"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>

<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_body">
	  
      </div>
    </div>
  </div>
</div>


<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>

	$( function () {

		$('body').on('click', '.create-btn', function (e) {
			e.preventDefault();
			$('#createModal').modal('show');
			
			$.ajax({
				url 		: '<?= base_url()?>index.php/users/showForm',
				type 		: 'POST',
				data 		: { action : 'action'},
				dataType 	: 'json',
				success 	: function (response) {
					$('#modal_body').html(response);
					//console.log(response);
				}
			});
		});

		$('body').on('click', '#btn-submit', function (e) {
			if ( $('#createFrom')[0].checkValidity() ) {
				e.preventDefault();
				$('#btn-submit').text('Please wait...');

				$.ajax({
				url 		: '<?= base_url()?>index.php/users/create',
				type 		: 'POST',
				data 		: { action : 'action'},
				dataType 	: 'json',
				success 	: function (response) {
					// $('#modal_body').html(response);
					console.log(response);
				}
			});
			}
		});

		// $('#createFrom').submit( function (e) {
		// 	e.preventDefault();
		// 	alert('hello');
		// });

	});

</script>

</body>
</html>
