<?php

class Users extends CI_Controller {

    public function index()
    {
        $this->load->view('header');
        $this->load->view('index');
    }

    public function showForm()
    {
        $response = $this->load->view('createForm.php', '', true);
        echo json_encode($response);
    }

    public function create()
    {
        $this->load->model('User_model');
        //$this->form_validation->set_rules('db_field name', 'print empty error', 'empty')
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('user_name', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required[valid_email]');
        $this->form_validation->set_rules('mobile', 'Phone number', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ( $this->form_validation->run() == false ) {
            $this->showForm();
        } else {
            $formData = array();
            $formData['name']       = $this->input->post('name');
            $formData['user_name']  = $this->input->post('user_name');
            $formData['email']      = $this->input->post('email');
            $formData['mobile']     = $this->input->post('mobile');
            $formData['password']   = $this->input->post('password');
            $this->User_model->create( $formData );
            $this->session->set_flashdata('success', 'Record add successfully!');
            //redirect(base_url().'index.php/users/index');
            echo "success";
        }
        
    }
}


?>